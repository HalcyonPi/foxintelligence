const handleFormatJSON = (nodeObj) => {
  const objJSON = {
    status: nodeObj.find(key => key.status).status,
    result: {
      trips: [
        {
          code: nodeObj.find(key => key.code).code,
          name: nodeObj.find(key => key.name).name,
          details: {
            price: nodeObj.find(key => key.price).price,
            roundTrips: nodeObj.find(key => key.roundTrips).roundTrips,
          }
        }
      ],
      custom: {
        prices: nodeObj.find(key => key.prices).prices,
      }
    }
  };

  const strJSON = JSON.stringify(objJSON);

  return strJSON;
};

module.exports = handleFormatJSON;
