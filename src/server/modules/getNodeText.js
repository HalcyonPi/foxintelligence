const moment = require('moment');

const handleNodeFormat = require('./handleNodeFormat');

moment.locale('fr'); // Declare moment 'fr' environment

const status = ($) => {
  let nodeObj = {};
  const isStatusOK = !!$('.very-important').text();

  if (isStatusOK) nodeObj = { status: 'ok' };
  else nodeObj = { status: 'nok' };

  return nodeObj;
};

const customerInfo = ($) => {
  const code = $('span', '.pnr-ref').eq(-1).text().trim();
  const name = $('span', '.pnr-name').eq(-1).text().trim();

  return { code, name };
};

const totalPrice = ($) => {
  let price = $('.very-important').text().trim();
  price = handleNodeFormat.price(price);

  return { price };
};

const getPassengersArray = ($) => {
  let passengers = [];
  const context = $('.passengers').eq(0);

  let passengerAgeArray = [];

  $('tr', context).each((i, el) => {
    let age = $('.fare-details', el).prev().text().trim();
    const index = age.indexOf('(');
    age = age.slice(index);

    if (age.length > 0) passengerAgeArray = [...passengerAgeArray, age];
  });

  let passengerTypeArray = [];

  $('tr', context).each((i, el) => {
    const type = $('.fare-details', el).text().trim();
    const regex = /([Bb]illet échangeable)/;
    const test = type.match(regex);

    if (type.length > 0 && test) {
      passengerTypeArray = [...passengerTypeArray, 'échangeable'];
    } else if (type.length > 0 && !test) {
      passengerTypeArray = [...passengerTypeArray, 'non échangeable'];
    }
  });

  passengerAgeArray.forEach((age, i) => {
    passengers = [...passengers, {
      type: passengerTypeArray[i],
      age
    }];
  });

  return passengers;
};

const getTrainsArray = ($) => {
  let trains = [];
  let trainTypeArray = [];
  let trainNumberArray = [];

  const departureTimeArray = handleNodeFormat.time($('.origin-destination-hour.segment-departure').text().trim()).split('  ');

  const arrivalTimeArray = handleNodeFormat.time($('.origin-destination-hour.segment-arrival').text().trim()).split('  ');

  const departureStationArray = $('.origin-destination-station.segment-departure').text().trim()
    .split('  ');

  const arrivalStationArray = $('.origin-destination-station.segment-arrival').text().trim()
    .split('  ');

  $('.product-details').each((i, el) => {
    const trainType = $('td', el).eq(3).text().trim();
    const trainNumber = $('td', el).eq(4).text().trim();
    trainTypeArray = [...trainTypeArray, trainType];
    trainNumberArray = [...trainNumberArray, trainNumber];
  });

  const passengersArray = getPassengersArray($);

  trainNumberArray.forEach((number, i) => {
    const lastIndex = trainNumberArray.length - 1;

    if (lastIndex === i) {
      trains = [...trains, [{
        departureTime: departureTimeArray[i],
        departureStation: departureStationArray[i],
        arrivalTime: arrivalTimeArray[i],
        arrivalStation: arrivalStationArray[i],
        type: trainTypeArray[i],
        number,
        passengers: passengersArray
      }]];
    } else {
      trains = [...trains, [{
        departureTime: departureTimeArray[i],
        departureStation: departureStationArray[i],
        arrivalTime: arrivalTimeArray[i],
        arrivalStation: arrivalStationArray[i],
        type: trainTypeArray[i],
        number,
      }]];
    }
  });

  return trains;
};

const roundTripsArray = ($) => {
  let roundTrips = [];

  const typeArr = $('.travel-way').text().trim().split('  ');
  const tripDateArr = handleNodeFormat.date($);
  const trainsArray = getTrainsArray($);

  typeArr.forEach((el, i) => {
    roundTrips = [...roundTrips, { type: el, date: tripDateArr[i], trains: trainsArray[i] }];
  });

  return { roundTrips };
};

const pricesArray = ($) => {
  let prices = [];

  $('.product-header').each((i, el) => {
    const price = handleNodeFormat.price($('td', el).last().text().trim());
    prices = [...prices, { value: price }];
  });

  return { prices };
};

module.exports = {
  customerInfo,
  roundTripsArray,
  pricesArray,
  status,
  totalPrice,
};
