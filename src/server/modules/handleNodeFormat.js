const moment = require('moment');

const monthList = require('../_utils/monthList').module;

const getDateFromString = (str) => {
  const array = str.split(' ');

  const day = array.filter(el => el.match(/^[0-9]{2}$/)).join('');
  const month = array.filter(el => monthList.indexOf(el.toLowerCase()) > -1).join('');
  const year = array.filter(el => el.match(/^[0-9]{4}$/)).join('');

  return { day, month, year };
};

const getDateFromObj = ($) => {
  let dateArray = [];

  const orderDetails = $('tr', '.transaction-details').eq(2).text().trim();
  const { day, month, year } = getDateFromString(orderDetails);
  const orderDateUNIX = moment(`${day} ${month} ${year}`, 'DD MMM YYYY').valueOf();

  $('.product-travel-date').each((i, el) => {
    const dayMonth = $(el).text().trim();
    let date = `${dayMonth} ${year}`;
    const hour = $('.origin-destination-hour.segment-departure').eq(i).text().trim()
      .replace('h', ':');

    let dateUNIX = moment(`${date} ${hour}`, 'DD MMM YYYY HH:mm').valueOf();

    if (dateUNIX < orderDateUNIX) {
      const orderYearNum = parseInt(year, 10);
      const nextYear = (orderYearNum + 1).toString(10);
      date = `${dayMonth} ${nextYear}`;
      dateUNIX = moment(`${date} ${hour}`, 'DD MMM YYYY HH:mm').valueOf();
    }

    const dateISO = new Date(dateUNIX).toISOString();

    dateArray = [...dateArray, dateISO];
  });

  return dateArray;
};

const date = (data) => {
  let dateStr;
  const isString = typeof data === 'string';

  if (!isString) dateStr = getDateFromObj(data);
  else dateStr = getDateFromString(data);

  return dateStr;
};

const time = (str) => {
  const validHour = str.replace(/h/gi, ':');
  return validHour;
};

const price = (str) => {
  const validFormat = str.replace(/,/, '.').replace(/\s/, '');
  const priceNum = parseFloat(validFormat);

  return priceNum;
};

module.exports = { date, price, time };
