const cheerio = require('cheerio');
const minify = require('html-minifier').minify;

const handlePruneData = (data) => {
  const lightenHTML = data.replace(/((\\r\\n)|(\\))/gi, '');

  const $ = cheerio.load(lightenHTML, {
    normalizeWhitespace: false,
    decodeEntities: true,
  });

  const target = $('#main-column').html();

  const prunedData = minify(target, {
    removeAttributeQuotes: true,
    collapseWhitespace: true,
    conservativeCollapse: true,
  });

  return prunedData;
};

module.exports = handlePruneData;
