/*
STATIC ROUTES
*/
module.exports = (app, databasePath, staticPath) => {
  /*
  TEST RESULT
  */
  app.get('/test-fox', (request, response) => {
    response.status(200).sendFile(`${databasePath}/test-fox.json`, {
      headers: {
        'Content-Type': 'application/json',
      }
    }, (error) => {
      if (error) {
        response.status(500).send(error);
      }
    });
  });

  /*
  CATCH-ALL
  */
  app.get('/*', (request, response) => {
    response.status(200).sendFile(`${staticPath}/index.html`, {
      headers: {
        'Content-Type': 'text/html',
      }
    }, (error) => {
      if (error) {
        response.status(500).send(error);
      }
    });
  });
};
