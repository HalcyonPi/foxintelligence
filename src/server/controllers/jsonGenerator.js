const fs = require('fs');
const promisify = require('util').promisify;

const handlePruneData = require('../modules/handlePruneData');
const parser = require('./parser');
const handleFormatJSON = require('../modules/handleFormatJSON');

const readFileProm = promisify(fs.readFile);
const targetPath = './public/index.html';
const filePath = './src/database/test-fox.json';

const parseFile = (data) => {
  const prunedData = handlePruneData(data);
  const nodeObj = parser(prunedData);

  return nodeObj;
};

const buildJSON = (nodeObj) => {
  const validJSON = handleFormatJSON(nodeObj);

  return validJSON;
};

const createWriteFile = (json) => {
  const writeStream = fs.createWriteStream(filePath);
  writeStream.write(json);
  writeStream.end();
  console.log('Success! You may check the created file at the following path:', filePath);
};

readFileProm(targetPath, 'utf8')
  .then(fileContent => parseFile(fileContent))
  .then(nodeObj => buildJSON(nodeObj))
  .then((json) => {
    if (process.env.NODE_ENV === 'development') {
      console.log('File building operation is a success. You may now try: npm start');
    } else {
      createWriteFile(json);
    }
  })
  .catch(e => console.error(`Could not fulfilled the JSON generator process because: ${e}`));
