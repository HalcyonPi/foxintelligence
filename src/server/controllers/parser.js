const cheerio = require('cheerio');

const getNodeText = require('../modules/getNodeText');

const parser = (prunedData) => {
  let nodeArray = [];

  const $ = cheerio.load(prunedData, {
    normalizeWhitespace: false,
    decodeEntities: true,
  });

  nodeArray = [...nodeArray, getNodeText.status($)];
  nodeArray = [...nodeArray, getNodeText.customerInfo($)];
  nodeArray = [...nodeArray, getNodeText.totalPrice($)];
  nodeArray = [...nodeArray, getNodeText.roundTripsArray($)];
  nodeArray = [...nodeArray, getNodeText.pricesArray($)];

  return nodeArray;
};


module.exports = parser;
