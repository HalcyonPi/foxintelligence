require('dotenv').config();

const express = require('express');

const compression = require('compression');
const cors = require('cors'); // Allows CORS setting
const helmet = require('helmet'); // Used to strengthen HTTP headers security
const path = require('path');

/*
APP INIT
*/
const app = express();
const port = process.env.PORT || 3000; // Will be automatically set by Heroku

/*
COMPRESSION, HEADERS, CORS
*/
const corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.use(compression());
app.use(helmet());
app.use(cors(corsOptions));

/*
ROUTES
*/
const databasePath = path.join(__dirname, '../..', 'src/database');
const staticPath = path.join(__dirname, '../..', 'public');

app.use(express.static(databasePath));
app.use(express.static(staticPath));

require('./routes/statics')(app, databasePath, staticPath);

/*
TEST EXEC
*/
require('./controllers/jsonGenerator');

/*
LISTENER
*/
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
  console.log('env:', process.env.NODE_ENV);
});

module.exports = {
  app
};
