# foxintelligence-full-stack

@Goal: Generate a JSON from HTML data.

@User guide:
- Open the project root directory via your terminal
- Run the command: npm start
- Find the newly created json file in the ./src/database directory or served
at localhost:3000/test-fox

@Information:
- Find the parsed HTML file in the ./public directory or served at localhost:3000/
- localhost:3000/test-fox
